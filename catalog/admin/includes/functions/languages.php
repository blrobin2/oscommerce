<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
$db = new Db();

  function tep_get_languages_directory($code) {
    global $languages_id;

    $language_query = $db->tep_db_query("select languages_id, directory from " . TABLE_LANGUAGES . " where code = '" . $db->tep_db_input($code) . "'");
    if ($db->tep_db_num_rows($language_query)) {
      $language = $db->tep_db_fetch_array($language_query);
      $languages_id = $language['languages_id'];
      return $language['directory'];
    } else {
      return false;
    }
  }
?>