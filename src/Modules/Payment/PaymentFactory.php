<?php namespace Modules\Payment;


class PaymentFactory {

    /**
     * Hidden constructor.
     */
    private function __construct(){}

    /**
     * Create a new class in the Modules\Payment namespace.
     * @param $className
     * @return mixed
     */
    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
}