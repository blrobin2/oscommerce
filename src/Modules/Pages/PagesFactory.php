<?php namespace Modules\Pages;


class PagesFactory {

    private function __construct() {}

    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
} 