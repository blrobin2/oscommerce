<?php namespace Modules\ActionRecorder;

/**
 * Class ActionRecorderFactory
 * Creates the appropriate action recorder for a given page.
 *
 * @package Modules\ActionRecorder
 */
class ActionRecorderFactory {

    /**
     * The factory cannot be instantiated.
     */
    private function __construct(){}

    /**
     * Creates a new instance of an action recorder.
     * The __namespace__ constant resolves to the current namespace.
     *
     * @param $className
     * @return mixed
     */
    public static function create($className)
    {
        $class =  __NAMESPACE__.'\\'.$className;

        return new $class;
    }
} 