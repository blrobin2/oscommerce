<?php namespace Modules\Content;


class ContentFactory {

    /**
     * Hidden constructor
     */
    private function __construct(){}

    public static function create($className, $groupName)
    {
        // Convert this_format to ThisFormat
        $group = ucfirst(str_replace('_','', $groupName));

        $class = __NAMESPACE__.'\\'.$group.'\\'.$className;

        return new $class;
    }
} 