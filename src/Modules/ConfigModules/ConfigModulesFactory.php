<?php namespace Modules\ConfigModules;


class ConfigModulesFactory {

    /**
     * Can't construct factory
     */
    private function __construct(){}

    public static function create($moduleName)
    {
        $module = __NAMESPACE__.'\\'.$moduleName;

        return new $module;
    }
}