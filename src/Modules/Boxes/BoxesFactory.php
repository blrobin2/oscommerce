<?php namespace Modules\Boxes;


class BoxesFactory {

    /**
     * No construction.
     */
    private function __construct(){}

    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
} 