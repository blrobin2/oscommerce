<?php namespace Modules\Dashboard;


class DashboardModuleFactory {

    /**
     * Creates a new instance of a dashboard module.
     * __NAMESPACE__ resolves to the current namespace.
     *
     * @param $className
     * @return mixed
     */
    public static function create($className)
    {
        $class = __NAMESPACE__."\\".$className;

        return new $class;
    }
} 