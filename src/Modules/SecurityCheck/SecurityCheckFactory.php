<?php namespace Modules\SecurityCheck;


class SecurityCheckFactory {

    /**
     * Constructor not publicly available.
     */
    private function __construct() {}


    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
}