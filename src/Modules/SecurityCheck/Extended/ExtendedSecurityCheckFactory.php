<?php namespace Modules\SecurityCheck\Extended;


class ExtendedSecurityCheckFactory {

    private function __construct(){}

    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
}