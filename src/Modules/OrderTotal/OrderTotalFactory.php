<?php namespace Modules\OrderTotal;


class OrderTotalFactory {

    /**
     * Hidden constructor.
     */
    private function __construct(){}


    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
}