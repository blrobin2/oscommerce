<?php namespace Modules\OrderTotal;
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  class ot_shipping {
    var $title, $output;

      /**
       * @var \Db
       */
     protected $db;

    function ot_shipping() {

        $this->db = new \Db();

      $this->code = 'ot_shipping';
      $this->title = module_order_total_shipping_title;
      $this->description = module_order_total_shipping_description;
      $this->enabled = ((module_order_total_shipping_status == 'true') ? true : false);
      $this->sort_order = module_order_total_shipping_sort_order;

      $this->output = array();
    }

    function process() {
      global $order, $currencies;

      if (module_order_total_shipping_free_shipping == 'true') {
        switch (module_order_total_shipping_destination) {
          case 'national':
            if ($order->delivery['country_id'] == store_country) $pass = true; break;
          case 'international':
            if ($order->delivery['country_id'] != store_country) $pass = true; break;
          case 'both':
            $pass = true; break;
          default:
            $pass = false; break;
        }

        if ( ($pass == true) && ( ($order->info['total'] - $order->info['shipping_cost']) >= module_order_total_shipping_free_shipping_over) ) {
          $order->info['shipping_method'] = free_shipping_title;
          $order->info['total'] -= $order->info['shipping_cost'];
          $order->info['shipping_cost'] = 0;
        }
      }

      $module = substr($globals['shipping']['id'], 0, strpos($globals['shipping']['id'], '_'));

      if (tep_not_null($order->info['shipping_method'])) {
        if ($globals[$module]->tax_class > 0) {
          $shipping_tax = tep_get_tax_rate($globals[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
          $shipping_tax_description = tep_get_tax_description($globals[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);

          $order->info['tax'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
          $order->info['tax_groups']["$shipping_tax_description"] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
          $order->info['total'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);

          if (display_price_with_tax == 'true') $order->info['shipping_cost'] += tep_calculate_tax($order->info['shipping_cost'], $shipping_tax);
        }

        $this->output[] = array('title' => $order->info['shipping_method'] . ':',
                                'text' => $currencies->format($order->info['shipping_cost'], true, $order->info['currency'], $order->info['currency_value']),
                                'value' => $order->info['shipping_cost']);
      }
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = $this->db->query("select configuration_value from " . table_configuration . " where configuration_key = 'module_order_total_shipping_status'");
        $this->_check = $this->db->num_rows($check_query);
      }

      return $this->_check;
    }

    function keys() {
      return array('module_order_total_shipping_status', 'module_order_total_shipping_sort_order', 'module_order_total_shipping_free_shipping', 'module_order_total_shipping_free_shipping_over', 'module_order_total_shipping_destination');
    }

    function install() {
      $this->db->query("insert into " . table_configuration . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('display shipping', 'module_order_total_shipping_status', 'true', 'do you want to display the order shipping cost?', '6', '1','tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $this->db->query("insert into " . table_configuration . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('sort order', 'module_order_total_shipping_sort_order', '2', 'sort order of display.', '6', '2', now())");
      $this->db->query("insert into " . table_configuration . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('allow free shipping', 'module_order_total_shipping_free_shipping', 'false', 'do you want to allow free shipping?', '6', '3', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      $this->db->query("insert into " . table_configuration . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values ('free shipping for orders over', 'module_order_total_shipping_free_shipping_over', '50', 'provide free shipping for orders over the set amount.', '6', '4', 'currencies->format', now())");
      $this->db->query("insert into " . table_configuration . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('provide free shipping for orders made', 'module_order_total_shipping_destination', 'national', 'provide free shipping for orders sent to the set destination.', '6', '5', 'tep_cfg_select_option(array(\'national\', \'international\', \'both\'), ', now())");
    }

    function remove() {
      $this->db->query("delete from " . table_configuration . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }
  }
?>
