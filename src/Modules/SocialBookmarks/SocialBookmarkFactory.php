<?php namespace Modules\SocialBookmarks;


class SocialBookmarkFactory {

    /**
     * Disable public constructor
     */
    public function __construct(){}

    public static function create($className)
    {
        $class = __NAMESPACE__.'\\'.$className;

        return new $class;
    }
}