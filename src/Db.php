<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
*/

/**
 * Class Db
 * Mostly wrappers for MySQLi procedural code, with a couple of additional methods.
 * //Todo: extend mysqli and remove redundant methods.
 */
class Db {

    /**
     * The connection to the database.
     * @var mysqli
     */
    private $link;

    /**
     * Open a new connection to the MySQL server.
     *
     * @param string $server
     * @param string $username
     * @param string $password
     * @param string $database
     * @return mysqli
     */
    public function connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE)
    {
        $this->link = mysqli_connect($server, $username, $password, $database);

        if (!mysqli_connect_errno()) {
            mysqli_set_charset($this->link, 'utf8');
        }

        return $this->link;
    }

    /**
     * Close a previously opened database connection.
     *
     * @return bool
     */
    public function close()
    {
        return mysqli_close($this->link);
    }

    /**
     * Die and print the last connection error.
     *
     * @param $query
     * @param $errno
     * @param $error
     */
    public function error($query, $errno, $error)
    {
        if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
            error_log('ERROR: [' . $errno . '] ' . $error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
        }

        die('<font color="#000000"><strong>' . $errno . ' - ' . $error . '<br /><br />' . $query . '<br /><br /><small><font color="#ff0000">[TEP STOP]</font></small><br /><br /></strong></font>');
    }

    /**
     * Perform a query on the database.
     *
     * @param $query
     * @return bool|mysqli_result
     */
    public function query($query)
    {
        if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
            error_log('QUERY: ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
        }

        $result = mysqli_query($this->link, $query) or $this->error($query, mysqli_errno($this->link), mysqli_error($this->link));

        return $result;
    }

    /**
     * A helper function for performing inserts and updates on the database.
     *
     * @param $table
     * @param $data
     * @param string $action
     * @param string $parameters
     * @return bool|mysqli_result
     */
    public function perform($table, $data, $action = 'insert', $parameters = '')
    {
        reset($data);
        if ($action == 'insert') {
            $query = 'insert into ' . $table . ' (';
            while (list($columns,) = each($data)) {
                $query .= $columns . ', ';
            }
            $query = substr($query, 0, -2) . ') values (';
            reset($data);
            while (list(, $value) = each($data)) {
                switch ((string)$value) {
                    case 'now()':
                        $query .= 'now(), ';
                        break;
                    case 'null':
                        $query .= 'null, ';
                        break;
                    default:
                        $query .= '\'' . $this->escape_string($value) . '\', ';
                        break;
                }
            }
            $query = substr($query, 0, -2) . ')';
        } elseif ($action == 'update') {
            $query = 'update ' . $table . ' set ';
            while (list($columns, $value) = each($data)) {
                switch ((string)$value) {
                    case 'now()':
                        $query .= $columns . ' = now(), ';
                        break;
                    case 'null':
                        $query .= $columns .= ' = null, ';
                        break;
                    default:
                        $query .= $columns . ' = \'' . $this->escape_string($value) . '\', ';
                        break;
                }
            }
            $query = substr($query, 0, -2) . ' where ' . $parameters;
        }

        return $this->query($query);
    }

    /**
     * Fetch a result row as an associative array.
     *
     * @param $db_query
     * @return array|null
     */
    public function fetch_array($db_query)
    {
        return mysqli_fetch_array($db_query, MYSQLI_ASSOC);
    }

    /**
     * Get the number of rows in a result.
     *
     * @param $db_query
     * @return int
     */
    public function num_rows($db_query)
    {
        return mysqli_num_rows($db_query);
    }

    /**
     * Adjust the result pointed to an arbitrary row in the result.
     *
     * @param $db_query
     * @param $row_number
     * @return bool
     */
    public function data_seek($db_query, $row_number)
    {
        return mysqli_data_seek($db_query, $row_number);
    }

    /**
     * Returns the auto-generated ID used in the last query.
     *
     * @return int|string
     */
    public function insert_id()
    {
        return mysqli_insert_id($this->link);
    }

    /**
     * Free the memory associated with a result.
     *
     * @param $db_query
     */
    public function free_result($db_query)
    {
        return mysqli_free_result($db_query);
    }

    /**
     * Returns an array of object representing the fields in a result set.
     *
     * @param $db_query
     * @return bool|object
     */
    public function fetch_fields($db_query)
    {
        return mysqli_fetch_field($db_query);
    }

    /**
     * Convert special characters to HTML entities.
     *
     * @param $string
     * @return string
     */
    public function output($string)
    {
        return htmlspecialchars($string);
    }

    /**
     * Escapes special characters in a string for use in an SQL statement.
     * Takes into account the current charset of the connection.
     *
     * @param $string
     * @return string
     */
    public function escape_string($string)
    {
        return mysqli_real_escape_string($this->link, $string);
    }

    /**
     * An additional string sanitizer, used primarily for supergobals.
     *
     * @param $string
     * @return array
     */
    public function prepare_input($string)
    {
        if (is_string($string)) {
            return trim(tep_sanitize_string(stripslashes($string)));
        } elseif (is_array($string)) {
            reset($string);
            while (list($key, $value) = each($string)) {
                $string[$key] = $this->prepare_input($value);
            }
            return $string;
        } else {
            return $string;
        }
    }

    /**
     * Get the number of affected rows in a previous MySQL operation.
     *
     * @return int
     */
    public function affected_rows()
    {
        return mysqli_affected_rows($this->link);
    }

    /**
     * Returns the version of the MySQL server.
     *
     * @return string
     */
    public function server_info()
    {
        return mysqli_get_server_info($this->link);
    }
}