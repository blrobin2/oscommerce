<?php namespace Admin;

/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

use Modules\ActionRecorder\ActionRecorderFactory;
use actionRecorder;

class actionRecorderAdmin extends actionRecorder {

    function __construct($module, $user_id = null, $user_name = null)
    {
        global $language, $PHP_SELF;

        include(DIR_FS_CATALOG . 'includes/languages/' . $language . '/modules/action_recorder/' . $module . '.' . substr($PHP_SELF, (strrpos($PHP_SELF, '.') + 1)));

        $this->_module = $module;

        if ( ! empty($user_id) && is_numeric($user_id) )
        {
            $this->_user_id = $user_id;
        }

        if ( ! empty($user_name) )
        {
            $this->_user_name = $user_name;
        }

        $GLOBALS[ $this->_module ] = ActionRecorderFactory::create($module);
        $GLOBALS[ $this->_module ]->setIdentifier();
    }
}