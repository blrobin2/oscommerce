<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

use Modules\ConfigModules\ConfigModulesFactory;
  class cfg_modules {
    var $_modules = array();

      protected $configModules = [
          'cfgm_action_recorder',
          'cfgm_boxes',
          'cfgm_dashboard',
          'cfgm_header_tags',
          'cfgm_order_total',
          'cfgm_payment',
          'cfgm_shipping',
          'cfgm_social_bookmarks',
      ];

    public function __construct() {
      global $PHP_SELF, $language;

      foreach($this->configModules as $class){
          include(DIR_WS_LANGUAGES . $language . '/modules/cfg_modules/' . $class.'.php');

          $m = ConfigModulesFactory::create($class);

          $this->_modules[] = array('code' => $m->code,
              'directory' => $m->directory,
              'language_directory' => $m->language_directory,
              'key' => $m->key,
              'title' => $m->title,
              'template_integration' => $m->template_integration);
      }
    }

    function getAll() {
      return $this->_modules;
    }

    function get($code, $key) {
      foreach ($this->_modules as $m) {
        if ($m['code'] == $code) {
          return $m[$key];
        }
      }
    }

    function exists($code) {
      foreach ($this->_modules as $m) {
        if ($m['code'] == $code) {
          return true;
        }
      }

      return false;
    }
  }
?>
