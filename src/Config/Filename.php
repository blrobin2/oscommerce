<?php namespace Config;

/**
 * Class Filename
 * The names of files used in the application.
 *
 * @package Config
 * @todo render obsolete with MVC. References will likely be to routes, not files.
 */
class Filename {

    const ACCOUNT = 'account.php';
    const ACCOUNT_EDIT = 'account_edit.php';
    const ACCOUNT_HISTORY = 'account_history.php';
    const ACCOUNT_HISTORY_INFO = 'account_history_info.php';
    const ACCOUNT_NEWSLETTERS = 'account_newsletters.php';
    const ACCOUNT_NOTIFICATIONS = 'account_notifications.php';
    const ACCOUNT_PASSWORD = 'account_password.php';
    const ADDRESS_BOOK = 'address_book.php';
    const ADDRESS_BOOK_PROCESS = 'address_book_process.php';
    const ADVANCED_SEARCH = 'advanced_search.php';
    const ADVANCED_SEARCH_RESULT = 'advanced_search_result.php';
    const ALSO_PURCHASED_PRODUCTS = 'also_purchased_products.php';
    const CHECKOUT_CONFIRMATION = 'checkout_confirmation.php';
    const CHECKOUT_PAYMENT = 'checkout_payment.php';
} 