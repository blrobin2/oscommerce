# README #

This is my refactor of OSCommerce to be a more modern, front-loaded, MVC architecture.

### What is this repository for? ###

* This is for my company primarily, but I leave it up for anyone to use.
* Version 0.0.3

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Payment Modules ###
To use Braintree, include in the `composer.json`:

    {
      "require" : {
        "braintree/braintree_php" : "2.38.0"
      }
    }

### Who do I talk to? ###

* BT <bt@kspcs.com>
* [OSCommerce](http://oscommerce.com/)